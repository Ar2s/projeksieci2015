# telnet program example
import socket, select, string, sys,base64
 
def prompt() :
    sys.stdout.write('')
    sys.stdout.flush()
 
import base64
def encode(key, clear):
    enc = []
    for i in range(len(clear)):
        key_c = key[i % len(key)]
        enc_c = chr((ord(clear[i]) + ord(key_c)) % 256)
        enc.append(enc_c)
    return base64.urlsafe_b64encode("".join(enc))

def decode(key, enc):
    dec = []
    enc = base64.urlsafe_b64decode(enc)
    for i in range(len(enc)):
        key_c = key[i % len(key)]
        dec_c = chr((256 + ord(enc[i]) - ord(key_c)) % 256)
        dec.append(dec_c)
    return "".join(dec)
    
#main function
if __name__ == "__main__":
     
    if(len(sys.argv) < 3) :
        print 'Usage : python telnet.py hostname port'
        sys.exit()
    
    host = sys.argv[1]
    port = int(sys.argv[2])
    wiadomosc = " "
    name = raw_input("Podaj swoja nazwe --> ")
    klucz = raw_input("podaj klucz komunikacji --> ")  
     
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.settimeout(2)
     
    # connect to remote host
    try :
        s.connect((host, port))
    except :
        print 'Unable to connect'
        sys.exit()
     
    print 'Connected to remote host. Start sending messages'
    prompt()
     
    while 1:
        socket_list = [sys.stdin, s]
         
        # Get the list sockets which are readable
        read_sockets, write_sockets, error_sockets = select.select(socket_list , [], [])
         
        for sock in read_sockets:
            #incoming message from remote server
            if sock == s:
                data = sock.recv(4096)
                if not data :
                    print '\nDisconnected from chat server'
                    sys.exit()
                else :
                    #print data
                    wiadomosc = decode(klucz,data)
                    sys.stdout.write(wiadomosc)
                    prompt()
             
            #user entered a message
            else :
                msg = sys.stdin.readline()
                wiadomosc = '<' + name + '> ' + msg
                wiadomosc = encode(klucz,wiadomosc)
                s.sendall(wiadomosc)
                prompt()
                