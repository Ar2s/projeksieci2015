import socket               # Import socket module
import sys
from LSBSteg import LSBSteg
import cv2.cv as cv
s = socket.socket()         # Create a socket object
host = socket.gethostname() # Get local machine name
port = 12345                 # Reserve a port for your service.
 

choice = raw_input("Type the number to: \n 1. hide text and send image \n 2.read message hidden \n 3. exit \n --> ")
while 1:
    if choice == "1":
        #Hide
        str = raw_input("Type what you want to hide and send --> " )
        carrier = cv.LoadImage("tosend.jpg")
        steg = LSBSteg(carrier)
        steg.hideText(str)
        steg.saveImage("tosendHidden.png") #Image that contain data
        
        
        s.connect((host, port))
        f = open('tosendHidden.png','rb')
        print 'Sending...'
        l = f.read(1024)
        while (l):
            print 'Sending...'
            s.send(l)
            l = f.read(1024)
        f.close()
        print "Done Sending"
        s.shutdown(socket.SHUT_WR)
        print s.recv(1024)
        s.close()                  # Close the socket when done
        choice = raw_input("Type the number to: \n 1. hide text and send image \n 2. read message hidden \n 3. exit \n --> ")
   
    elif choice == "2":
        #Unhide    
        im = cv.LoadImage("torecv.png")
        steg = LSBSteg(im)
        print "Text value:",steg.unhideText()
        choice = raw_input("Type the number to: \n 1. hide text and send image \n 2. read message hidden \n 3. exit \n --> ")
    
    elif choice == "3":
        print("Goodbye")
        sys.exit()