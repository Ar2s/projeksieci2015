Projekt sieci 2015

1. Opis: 
    Prosty komunikator sieciowy z szyfrowaniem.
    
2. Uruchamianie:
    serwera :
        python chat-serwer.py host port
    clienta:
        python chat-client.py host port
        
3. 
Czat prosi o podanie swojej nazwy oraz klucza komunikacji, gdy na chacie beda osoby o takim samym kluczu, beda one widzialy poprawne wiadomosci.
W przeciwnym razie beda widzialy bledne wiadomosci.


Steganography:
    imag-client.py potrafi ukryć wiadomosc w zdjeciu "tosend.jpg", przy użyciu LSBteg.py, ukrywając wiadomość w najmniej znaczących bitach.
    client wysyła zdjęcie "tosendHidden.png" z ukrytą wiadomoscia do lokalnego serwera na porcie 12345.
    Serwer aktualnie tylko odbiera nadesłane zdjęcia i zapisuje je jako torecv.png
    client potrafi odczytac ukrytą wiadomosc w zdjeciu
